      REAL FUNCTION QUADQ*8(W,AA,BB,IN)
      IMPLICIT REAL*8(A-H,J-Z)
      DIMENSION X(800),Y(800),S(801)
c Numerical integration of function W from AA to BB; IN: discretization number
      DEL=BB-AA
      A=0.
      B=1.
      D=(B-A)/(IN-1)
      DA=D/10.D0
      DB=DA
      D=(B-DB-A-DA)/(IN-1)
      DO 10 I=1,IN
      X(I)=A+DA+D*(I-1)
      XX=X(I)
      Y(I)=W(AA+DEL*XX)
10    CONTINUE
      E=X(2)-X(1)
      S(1)=FITINT(X(1),E,Y(1),Y(2),Y(3),Y(4),Y(5),Y(6),A,X(1))
      S(2)=FITINT(X(1),E,Y(1),Y(2),Y(3),Y(4),Y(5),Y(6),X(1),X(2))
      S(3)=FITINT(X(1),E,Y(1),Y(2),Y(3),Y(4),Y(5),Y(6),X(2),X(3))
      S(IN-1)=FITINT(X(IN-5),E,Y(IN-5),Y(IN-4),Y(IN-3),Y(IN-2),Y(IN-1),
     $ Y(IN),X(IN-2),X(IN-1))
      S(IN)=FITINT(X(IN-5),E,Y(IN-5),Y(IN-4),Y(IN-3),Y(IN-2),Y(IN-1),
     $ Y(IN),X(IN-1),X(IN))
      S(IN+1)=FITINT(X(IN-5),E,Y(IN-5),Y(IN-4),Y(IN-3),Y(IN-2),Y(IN-1),
     $ Y(IN),X(IN),B)
      INM4=IN-4
      INP2=IN+2
      INP1=IN+1
      DO 25 II=2,INM4
      S(II+2)=FITINT(X(II-1),E,Y(II-1),Y(II),Y(II+1),Y(II+2),Y(II+3),
     $  Y(II+4),X(II+1),X(II+2))
25    CONTINUE
      QUADQ=0.D0
      DO 12 I=1,INP1
      QUADQ=QUADQ+S(I)
12    CONTINUE
      QUADQ=DEL*QUADQ
      RETURN
      END
      REAL FUNCTION QUAD1*8(W,AA,BB,IN)
      IMPLICIT REAL*8(A-H,J-Z)
      DIMENSION X(800),Y(800),S(801)
c Numerical integration of function W from AA to BB; IN: discretization number
      DEL=BB-AA
      A=0.
      B=1.
      D=(B-A)/(IN-1)
      DA=D/10.D0
      DB=DA
      D=(B-DB-A-DA)/(IN-1)
      DO 10 I=1,IN
      X(I)=A+DA+D*(I-1)
      XX=X(I)
      Y(I)=W(AA+DEL*XX)
10    CONTINUE
      E=X(2)-X(1)
      S(1)=FITINT(X(1),E,Y(1),Y(2),Y(3),Y(4),Y(5),Y(6),A,X(1))
      S(2)=FITINT(X(1),E,Y(1),Y(2),Y(3),Y(4),Y(5),Y(6),X(1),X(2))
      S(3)=FITINT(X(1),E,Y(1),Y(2),Y(3),Y(4),Y(5),Y(6),X(2),X(3))
      S(IN-1)=FITINT(X(IN-5),E,Y(IN-5),Y(IN-4),Y(IN-3),Y(IN-2),Y(IN-1),
     $ Y(IN),X(IN-2),X(IN-1))
      S(IN)=FITINT(X(IN-5),E,Y(IN-5),Y(IN-4),Y(IN-3),Y(IN-2),Y(IN-1),
     $ Y(IN),X(IN-1),X(IN))
      S(IN+1)=FITINT(X(IN-5),E,Y(IN-5),Y(IN-4),Y(IN-3),Y(IN-2),Y(IN-1),
     $ Y(IN),X(IN),B)
      INM4=IN-4
      INP2=IN+2
      INP1=IN+1
      DO 25 II=2,INM4
      S(II+2)=FITINT(X(II-1),E,Y(II-1),Y(II),Y(II+1),Y(II+2),Y(II+3),
     $  Y(II+4),X(II+1),X(II+2))
25    CONTINUE
      QUAD1=0.D0
      DO 12 I=1,INP1
      QUAD1=QUAD1+S(I)
12    CONTINUE
      QUAD1=DEL*QUAD1
      RETURN
      END
      REAL FUNCTION QUADE2*8(W,A,B,IN)
      IMPLICIT REAL*8(A-H,J-Z)
      DIMENSION X(800),Y(800),S(801)
      COMMON/COME2/E2
c   1-dimensional numerical integration of 2-dim. function W(E2,X) over X 
c  from AA to BB, for fixed E2; IN: discretization number
      D=(B-A)/(IN-1)
      DA=D/10.D0
      DB=DA
      D=(B-DB-A-DA)/(IN-1)
      DO 10 I=1,IN
      X(I)=A+DA+D*(I-1)
      XX=X(I)
      Y(I)=W(E2,XX)
10    CONTINUE
      E=X(2)-X(1)
      S(1)=FITINT(X(1),E,Y(1),Y(2),Y(3),Y(4),Y(5),Y(6),A,X(1))
      S(2)=FITINT(X(1),E,Y(1),Y(2),Y(3),Y(4),Y(5),Y(6),X(1),X(2))
      S(3)=FITINT(X(1),E,Y(1),Y(2),Y(3),Y(4),Y(5),Y(6),X(2),X(3))
      S(IN-1)=FITINT(X(IN-5),E,Y(IN-5),Y(IN-4),Y(IN-3),Y(IN-2),Y(IN-1),
     $ Y(IN),X(IN-2),X(IN-1))
      S(IN)=FITINT(X(IN-5),E,Y(IN-5),Y(IN-4),Y(IN-3),Y(IN-2),Y(IN-1),
     $ Y(IN),X(IN-1),X(IN))
      S(IN+1)=FITINT(X(IN-5),E,Y(IN-5),Y(IN-4),Y(IN-3),Y(IN-2),Y(IN-1),
     $ Y(IN),X(IN),B)
      INM4=IN-4
      INP2=IN+2
      INP1=IN+1
      DO 25 II=2,INM4
      S(II+2)=FITINT(X(II-1),E,Y(II-1),Y(II),Y(II+1),Y(II+2),Y(II+3),
     $  Y(II+4),X(II+1),X(II+2))
25    CONTINUE
      QUADE2=0.D0
      DO 12 I=1,INP1
      QUADE2=QUADE2+S(I)
12    CONTINUE
      RETURN
      END
      REAL FUNCTION QUADT*8(W,A,B,IN)
      IMPLICIT REAL*8(A-H,J-Z)
      DIMENSION X(800),Y(800),S(801)
      COMMON/COMT/T
c   1-dimensional numerical integration of 2-dim. function W(X,T) over X 
c  from AA to BB, for fixed T; IN: discretization number
      D=(B-A)/(IN-1)
      DA=D/10.D0
      DB=DA
      D=(B-DB-A-DA)/(IN-1)
      DO 10 I=1,IN
      X(I)=A+DA+D*(I-1)
      XX=X(I)
      Y(I)=W(XX,T)
10    CONTINUE
      E=X(2)-X(1)
      S(1)=FITINT(X(1),E,Y(1),Y(2),Y(3),Y(4),Y(5),Y(6),A,X(1))
      S(2)=FITINT(X(1),E,Y(1),Y(2),Y(3),Y(4),Y(5),Y(6),X(1),X(2))
      S(3)=FITINT(X(1),E,Y(1),Y(2),Y(3),Y(4),Y(5),Y(6),X(2),X(3))
      S(IN-1)=FITINT(X(IN-5),E,Y(IN-5),Y(IN-4),Y(IN-3),Y(IN-2),Y(IN-1),
     $ Y(IN),X(IN-2),X(IN-1))
      S(IN)=FITINT(X(IN-5),E,Y(IN-5),Y(IN-4),Y(IN-3),Y(IN-2),Y(IN-1),
     $ Y(IN),X(IN-1),X(IN))
      S(IN+1)=FITINT(X(IN-5),E,Y(IN-5),Y(IN-4),Y(IN-3),Y(IN-2),Y(IN-1),
     $ Y(IN),X(IN),B)
      INM4=IN-4
      INP2=IN+2
      INP1=IN+1
      DO 25 II=2,INM4
      S(II+2)=FITINT(X(II-1),E,Y(II-1),Y(II),Y(II+1),Y(II+2),Y(II+3),
     $  Y(II+4),X(II+1),X(II+2))
25    CONTINUE
      QUADT=0.D0
      DO 12 I=1,INP1
      QUADT=QUADT+S(I)
12    CONTINUE
      RETURN
      END
      REAL FUNCTION FITINT*8(X1,E,Y1,Y2,Y3,Y4,Y5,Y6,A,B)
      IMPLICIT REAL*8(A-H,J-Z)
      Z2=Y2-Y1
      Z3=Y3-Y1
      Z4=Y4-Y1
      Z5=Y5-Y1
      Z6=Y6-Y1
      C3=Z3-2.D0*Z2
      C4=Z4-3.D0*Z2
      C5=Z5-4.D0*Z2
      C6=Z6-5.D0*Z2
      D4=C4-3.D0*C3
      D5=C5-6.D0*C3
      D6=C6-10.D0*C3
      F5=D5-4.D0*D4
      F6=D6-10.D0*D4
      A5=(F6-5.D0*F5)/(190.D0*E**5)
      A4=(F6-1180.D0*E**5*A5)/(120.D0*E**4)
      A3=(D6-480.D0*E**4*A4-2890.D0*E**5*A5)/(60.D0*E**3)
      A2=(C6-120.D0*E**3*A3-620.D0*E**4*A4-3120.D0*E**5*A5)/(20.D0*E**2)
      A1=(Z2-E**2*A2-E**3*A3-E**4*A4-E**5*A5)/E
      A0=Y1
      G=A-X1
      H=B-X1
      SA=A0*G+1.D0/2.D0*A1*G**2+1.D0/3.D0*A2*G**3+1.D0/4.D0*A3*G**4+
     $ 1.D0/5.D0*A4*G**5+1.D0/6.D0*A5*G**6
      SB=A0*H+1.D0/2.D0*A1*H**2+1.D0/3.D0*A2*H**3+1.D0/4.D0*A3*H**4+
     $ 1.D0/5.D0*A4*H**5+1.D0/6.D0*A5*H**6
      FITINT=SB-SA
      RETURN
      END
CC2
c
c QUADQ, QUAD1:  1-dimensional numerical integration of 1-dim. function 
c        W(X)  from AA to BB; IN: discretization number
c  QUADE2:  1-dimensional numerical integration of 2-dim. function W(E2,X) over X 
c         from AA to BB, for fixed E2; IN: discretization number
c  QUADT:   1-dimensional numerical integration of 2-dim. function W(X,T) over X 
c         from AA to BB, for fixed T; IN: discretization number
c FITINT:  integral of high-order interpolation function


      IMPLICIT REAL*8(A-H,J-Z)
      COMMON/COMICB/ICB
      COMMON/COM1/M,M2,DEL,Z,MF,MGT,LAM
      COMMON/COM2/E2M,KSZI,A
      COMMON/COM3/CSOFT,IQ
C
c ICB: Coulomb correction parameter (ICB=1: no Coulomb correction)
c CSOFT: cutoff parameter for soft and hard bremsstrahlung integrals
c  (rad. corr. should not depend on this value, if it is small enough).
c IQ: numerical integration number 
      ICB=1
      CSOFT=0.0001
      IQ=50
C
      PRINT 1,ICB,CSOFT,IQ
1     FORMAT(/1X,'ICB=',I10,3X,'CSOFT=',F12.4,5X,'IQ=',I8)
C
      CALL NEUTRON
      CALL TABIN
      CALL TABOUT
      STOP
      END
      SUBROUTINE TABIN
      IMPLICIT REAL*8(A-H,J-Z)
      DIMENSION yin(14)
C RIN: relative radiative correction to Dalitz distribution (E2,T),
C      defined by eq. 3.1 in Phys. Rev. D 47, 2840 (1993)
C   --> rad. corr. of electron energy spectrum is subtracted.
C To compare with table 2 in that paper.
      yin(1)=0.01
      yin(2)=0.04
      yin(3)=0.07
      do j=4,7
         yin(j)=(j-3)*0.1
      enddo
      do j=8,11
         yin(j)=(j-2)*0.1
      enddo
      yin(12)=0.93
      yin(13)=0.96
      yin(14)=0.99
c
      OPEN(9,FILE='dat/dalitzrin.txt')
      do i=1,9
         x=i*0.1
         do j=1,14
             r=rin(x,yin(j))
             WRITE(9,1) x,yin(j),r
         enddo
      enddo
      CLOSE(9)
1     FORMAT(1X,F16.1,F16.2,F16.2)
      RETURN
      END
      SUBROUTINE TABOUT
      IMPLICIT REAL*8(A-H,J-Z)
      DIMENSION yout(14)
C ROUT: relative radiative correction to Dalitz distribution (E2,T),
C      defined by: ROUT=WGAM(E2,T)/W0(E2,TMIN)*100
      yout(1)=0.01
      yout(2)=0.04
      yout(3)=0.07
      do j=4,7
         yout(j)=(j-3)*0.1
      enddo
      do j=8,11
         yout(j)=(j-2)*0.1
      enddo
      yout(12)=0.93
      yout(13)=0.96
      yout(14)=0.99
c
      OPEN(9,FILE='dat/dalitzrout.txt')
      do i=1,9
         xout=i*0.1
         do j=1,14
             r=rout(xout,yout(j))
             WRITE(9,1) xout,yout(j),r
         enddo
      enddo
      CLOSE(9)
1     FORMAT(1X,F16.1,F16.2,F16.2)
      RETURN
      END
      REAL FUNCTION RIN*8(X,YIN)
      IMPLICIT REAL*8(A-H,J-Z)
      COMMON/COM1/M,M2,DEL,Z,MF,MGT,LAM
      COMMON/COM2/E2M,KSZI,A
C X: electron energy parameter  (between 0 and 1);
c     total electron energy:    E2=M2+(E2M-M2)*X  (in MeV) (see eq. 2.6 in 1993 paper).
c YIN: recoil kinetic energy parameter (between 0 and 1);
C     recoil kinetic energy:  T=TMIN+(TMAX-TMIN)*YIN  (in MeV)
C RIN: relative radiative correction to Dalitz distribution (E2,T),
C      defined by eq. 3.1 in Phys. Rev. D 47, 2840 (1993) --> r(x,y)=RIN
C   --> rad. corr. of electron energy spectrum is subtracted.
C To be compared with table 2 in that paper (z corresponds to YIN).
      E2=M2+(E2M-M2)*X
      P2=DSQRT(E2**2-M2**2)
      TMIN=(DEL-E2-P2)**2/(2.*(M-E2-P2))
      TMAX=(DEL-E2+P2)**2/(2.*(M-E2+P2))
      T=TMIN+(TMAX-TMIN)*YIN
      RIN=WGAM(E2,T)/W0(E2,T)*100.-SIRLIN(E2)*100.
      RETURN
      END
      REAL FUNCTION ROUT*8(XOUT,YOUT)
      IMPLICIT REAL*8(A-H,J-Z)
      COMMON/COM1/M,M2,DEL,Z,MF,MGT,LAM
      COMMON/COM2/E2M,KSZI,A
      COMMON/CRTOT/RTOT
C XOUT: electron energy parameter  (between 0 and 1);
c     total electron energy:    E2=M2+(E2H-M2)*XOUT.
C  E2H: see eq. 3.8 in Phys. Rev. D 47, 2840 (1993)
C   --> T=0 recoil kin. energy point of the Dalitz plot lower boundary TMIN(E2).
c YOUT: recoil kinetic energy parameter (between 0 and 1);
C     recoil kinetic energy:  T=TMIN*YOUT
C ROUT: relative radiative correction to Dalitz distribution (E2,T),
C      defined by: ROUT=WGAM(E2,T)/W0(E2,TMIN(E2))*100
      E2H=0.5*(DEL+M2**2/DEL)
      E2=M2+(E2H-M2)*XOUT
      P2=DSQRT(E2**2-M2**2)
      TMIN=(DEL-E2-P2)**2/(2.*(M-E2-P2))
      T=TMIN*YOUT
      ROUT=WGAM(E2,T)/W0(E2,TMIN)*100.
c      print 1,e2,tmin*1.d6,W0(E2,TMIN)
c1     FORMAT(/1X,'e2,tmin,w0=',2F12.4,d14.3 )
      RETURN
      END
      REAL FUNCTION W0*8(E2,T)
      IMPLICIT REAL*8(A-H,J-Z)
      COMMON/COM1/M,M2,DEL,Z,MF,MGT,LAM
      COMMON/COM2/E2M,KSZI,A
      PARAMETER (PI=3.14159265358979D0)
c W0(E2,T): zeroth-order Dalitz distribution  in
c       infinite nucleon mass approximation.
c     E2: electron total energy, T: recoil kinetic energy (both in MeV)
      P2=DSQRT(E2**2-M2**2)
      BETA=P2/E2
      E1=DEL-E2-T
      TM=(DEL**2-M2**2)/(2.*M)
      W0=M*KSZI/(4.*PI**3)*((1.+A)*E1*E2+A*M*(T-TM))*FC(E2)
      RETURN
      END
      REAL FUNCTION SIRLIN*8(E2)
      IMPLICIT REAL*8(A-H,J-Z)
      COMMON/COM1/M,M2,DEL,Z,MF,MGT,LAM
      COMMON/COM2/E2M,KSZI,A
      PARAMETER (PI=3.14159265358979D0)
c Analytical order-alpha relative rad. corr. to electron energy spectrum
c E2: electron total energy in MeV
      MP=938.28
      ALFA=1./137.036
      P2=DSQRT(E2**2-M2**2)
      B=P2/E2
      N=1./2.*DLOG((1.D0+B)/(1.D0-B))
      G=3.*DLOG(MP/M2)-3./4.+4.*(N/B-1.)*((E2M-E2)/(3.*E2)-3./2.+
     . DLOG(2.D0*(E2M-E2)/M2))+4./B*L(2.D0*B/(1.D0+B))+
     . N/B*(2.*(1.+B**2)+(E2M-E2)**2/(6.*E2**2)-4.*N)
      SIRLIN=ALFA/(2.*PI)*G
      RETURN
      END
      SUBROUTINE NEUTRON
      IMPLICIT REAL*8(A-H,J-Z)
      COMMON/COM1/M,M2,DEL,Z,MF,MGT,LAM
      COMMON/COM2/E2M,KSZI,A
c M2: electron mass; MD: proton mass; M: neutron mass; DEL=M-MD (in MeV)
c MF, MGT: fermi and Gamow-Teller matrix elements
c LAM: lambda=ga/gv
c E2M: maximum of electron total energy E2
c A: electron-neutrino correlation parameter
c Z: charge of recoil particle (proton)
      M2=0.510999D0
      DEL=1.29332
      MD=938.279
      M=MD+DEL
      Z=1.
      MF=1.
      MGT=DSQRT(3.D0)
      LAM=-1.27
C
      E2M=DEL-(DEL**2-M2**2)/(2.*M)
      KSZI=MF**2+LAM**2*MGT**2
      A=(MF**2-1./3.*LAM**2*MGT**2)/KSZI
c  test:
      E2H=0.5*(DEL+M2**2/DEL)
      E2=E2H
      P2=DSQRT(E2**2-M2**2)
      TMIN=(DEL-E2-P2)**2/(2.*(M-E2-P2))
      TMAX=(DEL-E2+P2)**2/(2.*(M-E2+P2))
      print 1,e2m-m2,e2h-m2,tmin*1.d6,tmax*1.d6
1     FORMAT(/1X,'e2m-m2,e2h-m2=',2F12.4,4x,'tmin,tmax=',2F16.4 )
      RETURN
      END
CC1
C
      include "RadCorrCb.f"
      include "Quad.f"
C
c
c Common variables:
c M2: electron mass; MD: proton mass; M: neutron mass; DEL=M-MD (in MeV)
c MF, MGT: fermi and Gamow-Teller matrix elements
c LAM: lambda=ga/gv
c E2M: maximum of electron total energy E2
c A: electron-neutrino correlation parameter
c Z: charge of recoil particle (proton)
c
c Subroutines:
c TABIN, TABOUT: tabulation of order-alpha radiative correction to Dalitz distribution (E2,T)
C RIN(X,YIN): relative radiative correction to Dalitz distribution (E2,T) inside Dalitz plot
C ROUT(X,YOUT): relative radiative correction to Dalitz distribution (E2,T) outside Dalitz plot
c W0(E2,T): zeroth-order part of the Dalitz distribution (E2, T) in
c       infinite nucleon mass (INM) approximation
c SIRLIN(E2): analytical order-alpha rad. corr. to electron energy spectrum
c NEUTRON: calc. of input parameters for neutron decay


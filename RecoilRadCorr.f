      IMPLICIT REAL*8(A-H,J-Z)
      COMMON/COMICB/ICB
      COMMON/COM1/M,M2,DEL,Z,MF,MGT,LAM
      COMMON/COM2/E2M,KSZI,A
      COMMON/COM3/CSOFT,IQ/COMIE2/IE2
C
c ICB: Coulomb correction parameter (ICB=1: no Coulomb correction)
c CSOFT: cutoff parameter for soft and hard bremsstrahlung integrals
c  (rad. corr. should not depend on this value, if it is small enough).
c IQ, IE2: numerical integration numbers 
      ICB=1
      CSOFT=0.0001
      IQ=50
      IE2=100
C
      PRINT 1,ICB,CSOFT,IQ,IE2
1     FORMAT(/1X,'ICB=',I10,3X,'CSOFT=',F12.4,5X,'IQ,IE2=',2I8)
C
      CALL NEUTRON
      CALL TAB1
      STOP
      END
      SUBROUTINE TAB1
      IMPLICIT REAL*8(A-H,J-Z)
      EXTERNAL WE0,WEG
      DIMENSION Y(300),RCTAB(300)
      COMMON/COM1/M,M2,DEL,Z,MF,MGT,LAM
      COMMON/COM2/E2M,KSZI,A
      COMMON/CRTOT/RTOT
      PARAMETER (PI=3.14159265358979D0,ALFA=1./137.036D0)
C
C semianalytical rad. corr. to total decay rate
      R0=QUADQ(WE0,M2,E2M,500)
      RGAN=QUADQ(WEG,M2,E2M,500)
      RTOT=RGAN/R0*100.
C
C Recoil spectrum rad. corr. calc.:
      DO I=1,80
         Y(I)=0.01*I
         RCTAB(I)=RC(Y(I))
      ENDDO
      X0=-DLOG(0.2D0)
      DO I=1,220
         X=X0+(10.-X0)/220.*I
         Y(80+I)=1.-DEXP(-X)
         RCTAB(80+I)=RC(Y(80+I))
      ENDDO
C
      OPEN(9,FILE='neutron.txt')
      Tm=(del**2-m2**2)/(2.*m);
      DO I=1,300
c        T=y(i)*Tm
          WRITE(9,1) Y(I),RCTAB(I)
c        WRITE(9,1) T*1.d6,RCTAB(I)
      ENDDO
      CLOSE(9)
1     FORMAT(1X,F16.4,F16.4)
      RETURN
      END
      REAL FUNCTION RC*8(Y)
      IMPLICIT REAL*8(A-H,J-Z)
      COMMON/COM1/M,M2,DEL,Z,MF,MGT,LAM
      COMMON/CRTOT/RTOT
c Y: relative recoil kinetic energy (between 0 and 1)
c TM: maximum of recoil kinetic energy; T: recoil kinetic energy (in MeV)
c RC: relative order-alpha radiative correction to
c  recoil spectrum; total decay rate relative rad. corr.  RTOT is subtracted
      TM=(DEL**2-M2**2)/(2.*M)
      T=TM*Y
      RC=WRG(T)/WR0(T)*100.-RTOT
      RETURN
      END
      REAL FUNCTION W0*8(E2,T)
      IMPLICIT REAL*8(A-H,J-Z)
      COMMON/COM1/M,M2,DEL,Z,MF,MGT,LAM
      COMMON/COM2/E2M,KSZI,A
      PARAMETER (PI=3.14159265358979D0)
c W0(E2,T): zeroth-order Dalitz distribution  in
c       infinite nucleon mass approximation.
c     E2: electron total energy, T: recoil kinetic energy (both in MeV)
      P2=DSQRT(E2**2-M2**2)
      BETA=P2/E2
      E1=DEL-E2-T
      TM=(DEL**2-M2**2)/(2.*M)
      W0=M*KSZI/(4.*PI**3)*((1.+A)*E1*E2+A*M*(T-TM))*FC(E2)
      RETURN
      END
      REAL FUNCTION WE0*8(E2)
      IMPLICIT REAL*8(A-H,J-Z)
      COMMON/COM1/M,M2,DEL,Z,MF,MGT,LAM
      COMMON/COM2/E2M,KSZI,A
      PARAMETER (PI=3.14159265358979D0)
c WE0(E2):  zeroth-order electron energy spectrum in INM approx., with Coulomb corr.
c E2: electron total energy in MeV
      P2=DSQRT(E2**2-M2**2)
      BETA=P2/E2
      WE0=KSZI/(2.*PI**3)*BETA*E2**2*(E2M-E2)**2*FC(E2)
      RETURN
      END
      REAL FUNCTION SIRLIN*8(E2)
      IMPLICIT REAL*8(A-H,J-Z)
      COMMON/COM1/M,M2,DEL,Z,MF,MGT,LAM
      COMMON/COM2/E2M,KSZI,A
      PARAMETER (PI=3.14159265358979D0)
c Analytical order-alpha rad. corr. to electron energy spectrum
c E2: electron total energy in MeV
      MP=938.28
      ALFA=1./137.036
      P2=DSQRT(E2**2-M2**2)
      B=P2/E2
      N=1./2.*DLOG((1.D0+B)/(1.D0-B))
      G=3.*DLOG(MP/M2)-3./4.+4.*(N/B-1.)*((E2M-E2)/(3.*E2)-3./2.+
     . DLOG(2.D0*(E2M-E2)/M2))+4./B*L(2.D0*B/(1.D0+B))+
     . N/B*(2.*(1.+B**2)+(E2M-E2)**2/(6.*E2**2)-4.*N)
      SIRLIN=ALFA/(2.*PI)*G
      RETURN
      END
      REAL FUNCTION WEG*8(E2)
      IMPLICIT REAL*8(A-H,J-Z)
c Analytical rad. corr. part of electron energy spectrum
c E2: electron total energy in MeV
      WEG=SIRLIN(E2)*WE0(E2)
      RETURN
      END
      REAL FUNCTION WR0*8(T)
      IMPLICIT REAL*8(A-H,J-Z)
      EXTERNAL W0
      COMMON/COM1/M,M2,DEL,Z,MF,MGT,LAM
      COMMON/COMT/XT
c Zeroth-order recoil energy spectrum
c      T: recoil kinetic energy (in MeV)
      XT=T
      MASSF=M-DEL
      PF=DSQRT(2.*MASSF*T+T**2)
      E2MIN=((DEL-T-PF)**2+M2**2)/(2.*(DEL-T-PF))
      E2MAX=((DEL-T+PF)**2+M2**2)/(2.*(DEL-T+PF))
C
      WR0=QUADT(W0,E2MIN,E2MAX,200)
      RETURN
      END
      REAL FUNCTION WRG*8(T)
      IMPLICIT REAL*8(A-H,J-Z)
      EXTERNAL WGAM
      COMMON/COM1/M,M2,DEL,Z,MF,MGT,LAM
      COMMON/COMT/XT/COMIE2/IE2
c Order-alpha radiative corr. of recoil energy spectrum
c      T: recoil kinetic energy (in MeV)
      XT=T
      TH=(DEL-M2)**2/(2.*(M-M2))
      MASSF=M-DEL
      PF=DSQRT(2.*MASSF*T+T**2)
      E2MIN=((DEL-T-PF)**2+M2**2)/(2.*(DEL-T-PF))
      E2MAX=((DEL-T+PF)**2+M2**2)/(2.*(DEL-T+PF))
C
      E2MED1=M2+(E2MIN-M2)*0.95
      E2MED2=E2MIN+(E2MAX-E2MIN)*0.05
      E2MED3=E2MIN+(E2MAX-E2MIN)*0.95
      IF(T.LT.TH) THEN
        WRG=QUADT(WGAM,M2,E2MED1,IE2)+
     .        QUADT(WGAM,E2MED1,E2MIN,IE2)+
     .        QUADT(WGAM,E2MIN,E2MED3,IE2)+
     .        QUADT(WGAM,E2MED3,E2MAX,IE2)
      ELSE
        WRG=QUADT(WGAM,E2MIN,E2MED2,IE2)+
     .        QUADT(WGAM,E2MED2,E2MED3,IE2)+
     .        QUADT(WGAM,E2MED3,E2MAX,IE2)
      ENDIF
      RETURN
      END
      SUBROUTINE NEUTRON
      IMPLICIT REAL*8(A-H,J-Z)
      COMMON/COM1/M,M2,DEL,Z,MF,MGT,LAM
      COMMON/COM2/E2M,KSZI,A
c M2: electron mass; MD: proton mass; M: neutron mass; DEL=M-MD (in MeV)
c MF, MGT: fermi and Gamow-Teller matrix elements
c LAM: lambda=ga/gv
c E2M: maximum of electron total energy E2
c A: electron-neutrino correlation parameter
c Z: charge of recoil particle (proton)
      M2=0.510999D0
      DEL=1.29332
      MD=938.279
      M=MD+DEL
      Z=1.
      MF=1.
      MGT=DSQRT(3.D0)
      LAM=-1.27
C
      E2M=DEL-(DEL**2-M2**2)/(2.*M)
      KSZI=MF**2+LAM**2*MGT**2
      A=(MF**2-1./3.*LAM**2*MGT**2)/KSZI
      RETURN
      END
CC1
C
      include "RadCorrCb.f"
      include "Quad.f"
C
c
c Common variables:
c M2: electron mass; MD: proton mass; M: neutron mass; DEL=M-MD (in MeV)
c MF, MGT: fermi and Gamow-Teller matrix elements
c LAM: lambda=ga/gv
c E2M: maximum of electron total energy E2
c A: electron-neutrino correlation parameter
c Z: charge of recoil particle (proton)
c
c Subroutines:
c TAB1: tabulation of order-alpha radiative correction to recoil energy spectrum
c RC(Y): relative order-alpha radiative correction to
c       recoil spectrum; total decay rate rad. corr. is subtracted
c W0(E2,T): zeroth-order part of the Dalitz distribution (E2, T) in
c       infinite nucleon mass (INM) approximation
c WE0(E2):  zeroth-order electron energy spectrum in INM approx., with Coulomb corr.
c SIRLIN(E2): analytical order-alpha rad. corr. to electron energy spectrum
c WEG(E2): analytical rad. corr. part of electron energy spectrum
c WR0(T):  zeroth-order recoil energy spectrum
c WRG(T):  order-alpha radiative corr. of recoil energy spectrum
c NEUTRON: calc. of input parameters for neutron decay


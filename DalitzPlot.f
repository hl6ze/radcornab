      IMPLICIT REAL*8(A-H,J-Z)
      COMMON/COM1/M,M2,DEL,Z,MF,MGT,LAM
      COMMON/COM2/E2M,KSZI,A
C
      CALL NEUTRON
      OPEN(9,FILE='dat/dalitzplot.txt')
      do i=0,1000
         x=i*0.001
         E2=M2+(E2M-M2)*X
         P2=DSQRT(E2**2-M2**2)
         TMIN=(DEL-E2-P2)**2/(2.*(M-E2-P2))
         TMAX=(DEL-E2+P2)**2/(2.*(M-E2+P2))
         WRITE(9,1) (e2-m2)*1.d3,tmin*1.d6,tmax*1.d6
      enddo
      CLOSE(9)
1     FORMAT(1X,F16.4,F16.4,F16.4)
      STOP
      END
      SUBROUTINE NEUTRON
      IMPLICIT REAL*8(A-H,J-Z)
      COMMON/COM1/M,M2,DEL,Z,MF,MGT,LAM
      COMMON/COM2/E2M,KSZI,A
c M2: electron mass; MD: proton mass; M: neutron mass; DEL=M-MD (in MeV)
c MF, MGT: fermi and Gamow-Teller matrix elements
c LAM: lambda=ga/gv
c E2M: maximum of electron total energy E2
c A: electron-neutrino correlation parameter
c Z: charge of recoil particle (proton)
      M2=0.510999D0
      DEL=1.29332
      MD=938.279
      M=MD+DEL
      Z=1.
      MF=1.
      MGT=DSQRT(3.D0)
      LAM=-1.27
C
      E2M=DEL-(DEL**2-M2**2)/(2.*M)
      KSZI=MF**2+LAM**2*MGT**2
      A=(MF**2-1./3.*LAM**2*MGT**2)/KSZI
c  test:
      E2H=0.5*(DEL+M2**2/DEL)
      E2=E2H
      P2=DSQRT(E2**2-M2**2)
      TMIN=(DEL-E2-P2)**2/(2.*(M-E2-P2))
      TMAX=(DEL-E2+P2)**2/(2.*(M-E2+P2))
      print 1,e2m-m2,e2h-m2,tmin*1.d6,tmax*1.d6
1     FORMAT(/1X,'e2m-m2,e2h-m2=',2F12.4,4x,'tmin,tmax=',2F16.4 )
      RETURN
      END
CC1
C

      REAL FUNCTION WGAM*8(E2,T)
      IMPLICIT REAL*8(A-H,J-Z)
      EXTERNAL MBRINT
      COMMON/COM1/M,M2,DEL,Z,MF,MGT,LAM
      COMMON/COM2/E2M,KSZI,A
      COMMON/COM3/CSOFT,IQ
      COMMON/COM4/XE2,XT,P2,PF,Q0,OM/CTMIN/TMIN
      PARAMETER (PI=3.14159265358979D0,ALFA=1./137.036D0)
c WGAM(E2,T): order-alpha radiative corr. part of Dalitz-distribution;
c     E2: electron total energy, T: recoil kinetic energy (both in MeV)
C Initial variable assignments:
c P2, PF: electron and proton momentum
c Q0, OM: see App. B in Phys. Rev. D 46, 2090 (1992)
      XE2=E2
      XT=T
      P2=DSQRT(E2**2-M2**2)
      TMIN=(DEL-E2-P2)**2/(2.*(M-E2-P2))
      TMAX=(DEL-E2+P2)**2/(2.*(M-E2+P2))
      MASSF=M-DEL
      PF=DSQRT(2.*MASSF*T+T**2)
      QMIN=DABS(P2-PF)
      Q0=DEL-E2-T
      OM=DMIN1(CSOFT*E2,CSOFT*Q0,(Q0-QMIN)/2.)
      CONST=1./(2.**9*PI**5*M)*FC(E2)
C R_out region:
      IF(T.LT.TMIN) THEN
        QMAX=P2+PF
        QMED1=QMIN+(QMAX-QMIN)*0.9
        QMED2=QMIN+(QMAX-QMIN)*0.99
        QMED3=QMIN+(QMAX-QMIN)*0.999
        WGAM=CONST*(QUADQ(MBRINT,QMIN,QMED1,IQ)+
     .   QUADQ(MBRINT,QMED1,QMED2,IQ)+
     .   QUADQ(MBRINT,QMED2,QMED3,IQ)+
     .   QUADQ(MBRINT,QMED3,QMAX,IQ))
        RETURN
      ENDIF
C R_in region:
C virtual + soft correction calculation:
      BETA=P2/E2
C     WE0=KSZI/(2.*PI**3)*BETA*E2**2*(E2M-E2)**2*FC(E2)
C     CEN=(M*(E2M-E2)-Q0*(M-E2))/(BETA*Q0*E2)
C     WECEN0=WE0/2.*(1.+A*BETA*CEN)
      MP=938.28
      N=1./2.*DLOG((1.D0+BETA)/(1.D0-BETA))
      ZVS=ALFA/PI*(3./2.*DLOG(MP/M2)+2./BETA*L(2.*BETA/(1.+BETA))-
     . 3./8.+2.*N/BETA*(1.-N)+2.*(N/BETA-1.)*DLOG(2.D0*OM/M2))
      WECENVS1=-ALFA*KSZI/(4.*PI**4)*(1.-BETA**2)*
     . N*(E2M-E2)**2*E2**2*FC(E2)
      WVS1=WECENVS1*M*(E2M-E2)/(BETA*E2*Q0**2)
      WVS=ZVS*W0(E2,T)+WVS1
C hard bremsstrahlung correction calculation:
      QMAX=Q0
      QMED1=QMIN+(QMAX-QMIN)*0.9
      QMED2=QMIN+(QMAX-QMIN)*0.99
      QMED3=QMIN+(QMAX-QMIN)*0.999
      WH=CONST*(QUADQ(MBRINT,QMIN,QMED1,IQ)+
     . QUADQ(MBRINT,QMED1,QMED2,IQ)+
     . QUADQ(MBRINT,QMED2,QMED3,IQ)+
     . QUADQ(MBRINT,QMED3,QMAX,IQ))
      WGAM=WVS+WH
      RETURN
      END
      REAL FUNCTION MBRINT*8(Q)
      IMPLICIT REAL*8(A-H,J-Z)
      COMMON/COM1/M,M2,DEL,Z,MF,MGT,LAM
      COMMON/COM2/E2M,KSZI,AEN
      COMMON/COM3/CSOFT,IQ
      COMMON/COM4/E2,T,P2,PF,Q0,OM/CTMIN/TMIN
      PARAMETER (PI=3.14159265358979D0,ALFA=1./137.036D0)
c MBRINT(Q):  bremsstrahlung photon matrix element squared;
c        see: Phys. Rev. D 46, 2090 (1992) -->  parameter Q: after eq. 3.2
C K_min, K_max calculation:
      KMIN=1.D0/2.D0*(Q0-Q)
      IF(T.GT.TMIN.AND.Q.GT.Q0-2.*OM) KMIN=OM
      KMAX=1.D0/2.D0*(Q0+Q)
C G_i (i=0,7) integral calculations:
      KA=KMIN
      KB=KMAX
      COSA=(P2**2-PF**2+Q**2)/2.D0/P2/Q
      A=M2**2+P2**2*(COSA**2+Q0**2/Q**2)+2.D0*E2*P2*COSA*Q0/Q
      B=Q0*(Q0**2-Q**2)/Q**2*P2**2+E2*P2*COSA*(Q0**2-Q**2)/Q
      C=P2**2*((Q0**2-Q**2)/2.D0/Q)**2
      ABC=4.D0*A*C-B**2
      SQRA=DSQRT(A*KA**2-B*KA+C)
      SQRB=DSQRT(A*KB**2-B*KB+C)
      SQR=SQRB-SQRA
      DSQRTA=DSQRT(A)
      DSQRTC=DSQRT(C)
C
      G0A=-1./DSQRTC*DLOG(DABS(2.*C-B*KA+2.*DSQRTC*SQRA)/KA)
      G0B=-1./DSQRTC*DLOG(DABS(2.*C-B*KB+2.*DSQRTC*SQRB)/KB)
      G0=G0B-G0A
      G1A=1.D0/DSQRTA*DLOG(DABS((2.D0*A*KA-B)/2.D0/
     . DSQRTA+SQRA))
      G1B=1.D0/DSQRTA*DLOG(DABS((2.D0*A*KB-B)/2.D0/
     . DSQRTA+SQRB))
      G1=G1B-G1A
      G2A=SQRA/A+B/2.D0/A*G1A
      G2B=SQRB/A+B/2.D0/A*G1B
      G2=G2B-G2A
      G3A=(2.D0*A*KA+3.D0*B)/4.D0/A**2*SQRA+
     . (3.D0*B**2-4.D0*A*C)/8.D0/A**2*G1A
      G3B=(2.D0*A*KB+3.D0*B)/4.D0/A**2*SQRB+
     . (3.D0*B**2-4.D0*A*C)/8.D0/A**2*G1B
      G3=G3B-G3A
      G4A=2.D0*(2.D0*A*KA-B)/ABC/SQRA
      G4B=2.D0*(2.D0*A*KB-B)/ABC/SQRB
      G4=G4B-G4A
      G5A=2.D0*(2.D0*C-B*KA)/(-ABC)/SQRA
      G5B=2.D0*(2.D0*C-B*KB)/(-ABC)/SQRB
      G5=G5B-G5A
      G6A=((4.D0*A*C-2.D0*B**2)*KA+2.D0*B*C)/
     . A/(-ABC)/SQRA+1.D0/A*G1A
      G6B=((4.D0*A*C-2.D0*B**2)*KB+2.D0*B*C)/
     . A/(-ABC)/SQRB+1.D0/A*G1B
      G6=G6B-G6A
      G7A=(A*ABC*KA**2-B*(10.D0*A*C-3.D0*B**2)*KA+
     . C*(8.D0*A*C-3.D0*B**2))/A**2/ABC/
     . SQRA+3.D0*B/2.D0/A**2*G1A
      G7B=(A*ABC*KB**2-B*(10.D0*A*C-3.D0*B**2)*KB+
     . C*(8.D0*A*C-3.D0*B**2))/A**2/ABC/
     . SQRB+3.D0*B/2.D0/A**2*G1B
      G7=G7B-G7A
C H0INT calculation:
      C0=-P2*(Q0**2-Q**2)/(2.*Q)*COSA
      C1=E2+P2*Q0/Q*COSA
      J1=1./KMIN-1./KMAX
      J2=C0*G4+C1*G5
      J3=G0
      F1=J1+M2**2*J2-2.*E2*J3
      J4=DLOG(KMAX/KMIN)
      J5=C0*G5+C1*G6
      J6=G1
      F2=J4+M2**2*J5-2.*E2*J6
      J7=KMAX-KMIN
      J8=C0*G6+C1*G7
      J9=G2
      F3=J7+M2**2*J8-2.*E2*J9
      J10=G3
      H0INT=-Q0*E2*F1+(E2-Q0)*F2+F3+Q0*J9-J10
C H1INT calculation:
      F4=C0*J1+C1*J4+M2**2*J6-2.*E2*J4
      N1K=1./2.*(Q0**2-Q**2)
      TM=(DEL**2-M2**2)/(2.*M)
      N12=M*(TM-T)
      H1INT=H0INT+N12*F1-N1K*F1-F4-N12*J6-E2*N1K*J3+Q0*J4+
     . M2**2*N1K*J2
      EE=4.*PI*ALFA
      MBRINT=16*KSZI*M**2*EE*(H0INT+AEN*H1INT)
      RETURN
      END
      REAL FUNCTION FC*8(E2)
      IMPLICIT REAL*8(A-H,J-Z)
      COMMON/COM1/M,M2,DEL,Z,MF,MGT,LAM
      COMMON/COMICB/ICB
      PARAMETER (PI=3.14159265358979D0)
c Coulomb correction of electron energy spectrum
c E2: electron total energy in MeV
        IF(ICB.EQ.1) THEN
      FC=1.
        ELSE
      ALF=1./137.036*Z
      P2=DSQRT(E2**2-M2**2)
      BETA=P2/E2
      AM=M/930.
      R=1.2*0.01/(4.*M2)*AM**0.333D0
C
C  COULOMB CORRECTION APPROXIMATION FORMULA OF WILKINSON
C    ( SEE APPENDIX OF 1982 NUCL. PH.A PAPER )
C
      G=DSQRT(1.-ALF**2)
      Y=ALF/BETA
      Y1=2./(1.+G)*Y
      A=2./(1.+G)
      LN=DLOG(Y1**2/(G**2+Y**2))+DLOG(PI/(Y1*DSINH(PI*Y1)))+
     . DLOG(1.+Y1**2)+(1.-G)*(2.-DLOG((1.+G)**2+Y**2)+
     . 2.*Y/(1.+G)*DATAN(Y/(1.+G))+(1./((1.+G)**2+Y**2))/6./A)-
     . 3.*DLOG(A)
      GAM=2.-9.82704D-5*Z**2+2.19D-9*Z**4
      IF(Y.LE.-10.D0) THEN
       FC=1.D-20
       RETURN
      ENDIF
      FC=2.*(1.+G)*(2.*P2*R)**(2.D0*(G-1.D0))/GAM**2*DEXP(PI*Y)*
     . DEXP(LN)
        ENDIF
      RETURN
      END
CC1
      REAL FUNCTION L*8(X)
      IMPLICIT REAL*8(A-H,J-Z)
      PARAMETER (PI=3.14159265358979D0)
C
C  Spence function for arbitrary X ;
C    L(X)=integral from 0 to X of DLOG(DABS(1.-T))/T.
C  This function subr. uses the LZHALF function subr.
C
      IF(X.GE.0.D0.AND.X.LE.0.5D0) L=LZHALF(X)
      IF(DABS(X).GE.1.D-18) THEN
        DX=DLOG(DABS(X))
      ELSE
        DX=0.
      ENDIF
      IF(DABS(1.-X).GE.1.D-18) THEN
        DX1=DLOG(DABS(1.-X))
      ELSE
        DX1=0.
      ENDIF
      DD=DX*DX1
      IF(X.GE.0.5D0.AND.X.LE.1.D0)
     . L=-LZHALF(1.-X)-PI**2/6.+DD
      IF(X.GE.1.D0.AND.X.LE.2.D0)
     . L=LZHALF((X-1.)/X)-PI**2/6.+DD-1./2.*DX**2
      IF(X.GE.2.D0) L=-LZHALF(1./X)-PI**2/3.+1./2.*DX**2
      IF(X.LE.0.D0.AND.X.GE.-1.D0)
     . L=-LZHALF(X/(X-1.))+1./2.*DX1**2
      IF(X.LE.-1.D0) L=LZHALF(1./(1.-X))+PI**2/6.+
     . DD-1./2.*DX1**2
      RETURN
      END
      REAL FUNCTION LZHALF*8(X)
      IMPLICIT REAL*8(A-H,J-Z)
C
C  SPENCE FUNCTION FOR X.LE.0.5 AND X.GE.0
C
      IF(X.LE.1.D-15) THEN
        LZHALF=-X
        RETURN
      ENDIF
      GX=0.
      S=X/4.
        DO I=1,50
      N=I
      GX=GX+S
      IF(S/GX.LE.1.D-15) GO TO 1
      S=S*X*N**2/(N+2.)**2
        ENDDO
1     LZHALF=-(X*(3.+GX)+2.*(1.-X)*DLOG(DABS(1.-X)))/(1.+X)
      RETURN
      END
C
c
c Common variables:
c M2: electron mass; MD: proton mass; M: neutron mass; DEL=M-MD (in MeV)
c MF, MGT: fermi and Gamow-Teller matrix elements
c LAM: lambda=ga/gv
c E2M: maximum of electron total energy E2
c A: electron-neutrino correlation parameter
c Z: charge of recoil particle (proton)
c ICB: Coulomb correction parameter (ICB=1: no Coulomb correction)
c CSOFT: cutoff parameter for soft and hard bremsstrahlung integrals
c  (rad. corr. should not depend on this value, if it is small enough).
c IQ, IE2: numerical integration numbers 
c
c Subroutines:
c WGAM(E2,T): order-alpha radiative corr. part of Dalitz-distribution;
c     E2: electron total energy, T: recoil kinetic energy (both in MeV)
c MBRINT(Q):  bremsstrahlung photon matrix element squared;
c        see: Phys. Rev. D 46, 2090 (1992) -->  parameter Q: after eq. 3.2
c FC(E2):  Coulomb correction of electron energy spectrum
c L(X), LZHALF(X):  Spence function calculation

